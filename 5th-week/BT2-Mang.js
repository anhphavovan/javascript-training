var man = [
    {
        id: "1",
        name:"The Cosmo (Đen) Quần Short Khaki",
        code:"TC1025011BA",
        price: "250.000",
        image: "https://down-vn.img.susercontent.com/file/vn-11134207-7qukw-lezquri2tj7uc1"
        
    },
    {
        id: "2",
        name: "Quần Baggy Đen Sang Trọng QQ",
        code: "TC1025011BA",
        price: "398.000",
        image: "https://down-vn.img.susercontent.com/file/vn-11134207-7qukw-lezquri2tj7uc1"
    },
    {
        id: "3",
        name: "The Cosmo (Đen) Quần Short Kaki",
        code: "TC1025011BA",
        price: "300.000",
        image: "https://down-vn.img.susercontent.com/file/vn-11134207-7qukw-lezquri2tj7uc1"
    },
    {
        id: "4",
        name: "The Cosmo (Đen) Quần Short Kaki",
        code: "TC1025011BA",
        price: "300.000",
        image: "https://down-vn.img.susercontent.com/file/vn-11134207-7qukw-lgsu94wuqvoz55"
    }
];
var women = [
    {
        id: "1",
        name: "Váy Fashion",
        code: "TC1025011BA",
        price: "250.000",
        image:"https://down-vn.img.susercontent.com/file/vn-11134207-7r98o-lqnpupitmq0n16"
    },
    {
        id: "2",
        name: "Áo Thun Phối Váy Ngắn",
        code: "TC1025011BA",
        price: "320.000",
        image:"https://down-vn.img.susercontent.com/file/vn-11134207-7r98o-lo4vv22mgvlj0d"
    },
    {
        id: "3",
        name: "Áo Thun",
        code: "TC1025011BA",
        price: "200.000",
        image:"https://down-vn.img.susercontent.com/file/vn-11134207-7r98o-lqfn5sv25giae9"
    },
    {
        id: "4",
        name: "Áo Khoác",
        code: "TC1025011BA",
        price: "500.000",
        image:"https://down-vn.img.susercontent.com/file/sg-11134201-7rbkv-lpq2b8eufceo22"
    }
];
function listProducts(){
    for (let i = 0; i < man.length-1; i++) {
        var demo = '<div class="col-3">';
        demo += '<div class="card" style="width: 18rem; ">';
        demo += '<img src="'+ man[i].image+'" class="card-img-top" style= "height:400px;">';
        demo += '<div class="card-body">';
        demo += '<h5 class="card-title">'+man[i].name+'</h5>';
        demo += '<p class="card-text">'+man[i].price+'</p>';    
        demo += '<a href="#" class="btn btn-primary" onclick="order()">Đặt Mua</a>';
        demo += '</div>';
        demo += '</div>';
        demo += '</div>';
        console.log(demo);
        document.getElementById("man").innerHTML   += demo;           
    }
    for (let i = 0; i < women.length-1; i++) {
        var demo = '<div class="col-3">';
        demo += '<div class="card" style="width: 18rem; ">';
        demo += '<img src="'+ women[i].image+'" class="card-img-top" style= "height:400px;">';
        demo += '<div class="card-body">';
        demo += '<h5 class="card-title">'+women[i].name+'</h5>';
        demo += '<p class="card-text">'+women[i].price+'</p>';    
        demo += '<a href="#" class="btn btn-primary" onclick="order()">Đặt Mua</a>';
        demo += '</div>';
        demo += '</div>';
        demo += '</div>';
        console.log(demo);
        document.getElementById("women").innerHTML   += demo;           
    }
}
listProducts();
